from __future__ import division

from math import sqrt
import numpy as np

def inner(x,y):
    return x.inner(y)

def norm(v):
    return v.norm('l2')

def transpmult(A, x):
    return A.transpmult(x)

eps = np.finfo(float).eps
