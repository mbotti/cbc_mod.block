#!/usr/bin/env python

from distutils.core import setup
setup(name = "cbc.block",
      version = "0.0.2",
      description = "Block utilities",
      author = "Joachim Berdal Haga",
      url = "http://www.launchpad.net/cbc.block/",
      packages = ["block", "block.algebraic", "block.algebraic.trilinos", "block.iterative", "block.algebraic.petsc"]
)

